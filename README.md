# How to run the Kinesis Source Connector

The connector will read messages from Kafka and publish them to the SQS queue named `scans`

## Prerequisites
You need to have installed:
- Docker and Docker compose
- [AWS CLI](https://aws.amazon.com/cli/)
- Credentials to access AWS, see [here](https://sites.google.com/masabi.com/platformitops/aws/aws-cli?pli=1#h.kpy4cmdzidsx) for more details. 

  In particular, when using docker-compose, it is assumed that you have the files `~/.aws/config` and `~/.aws/credentials/` configured as in the document.
- [jq](https://stedolan.github.io/jq/) to print in a more human-readable format what you get from the server responses.

## Run the connector in local with Docker-compose
- First, you need to build the `plugins` submodule and create an uber jar:
  ```bash
  ./gradlew shadowJar
  ```
- To run the connector and Kafka locally, using Docker Compose:
  ```bash
   docker-compose -f docker-compose.yml -f docker-compose-kafka-ui.yml -f docker-compose-kineses-source-connector.yml up -d --build --remove-orphans
  ```
  The command will automatically build the connector image for you. If you didn't create the uber jar beforehand, the image build will fail.

Once all containers are up and running:

- Verify the installation went ok, you should be able to see the connector `org.apache.camel.kafkaconnector.awskinesissource.CamelAwskinesissourceSourceConnector` from listed in the following response:
  - `curl localhost:8083/connector-plugins | jq` 

- Before configuring a connector, you need to create the topic `kineses-stream` in local and register the schema. An easy way to do it is using AKHQ.
  - Go to http://localhost:8080/ui/docker-kafka-server/topic/create and create the topic.
  
- Configure and launch the connector:
  ```shell
  curl -X POST -H  "Content-Type:application/json" --data "@./config.json" http://localhost:8083/connectors | jq
  ```
- Check if the connector now exists and runs correctly: 
  - To see if it's been configured [http://localhost:8083/connectors?expand=info](http://localhost:8083/connectors?expand=info)
  - To see its status [http://localhost:8083/connectors/scans-source-connector/status](http://localhost:8083/connectors/scans-source-connector/status)

- If you want to delete the connector and start again:
  ```shell
  curl -X DELETE http://localhost:8083/connectors/kineses-source-connector
  ```

The connector will try and consume from [this kinesis stream in integration](https://eu-west-1.console.aws.amazon.com/kinesis/home?region=eu-west-1#/streams/details/ew1-integration-datawarehouse-replay/).

An example of message sent to Kafka could be:
```json
{"queueId":"fdf7243b-a786-41e6-979c-25347148e573","eventJson":"{\"activationTimestamp\":1667772571831,\"appVersion\":\"3.20.0\",\"scanTime\":\"2022-11-06T22:16:11Z\",\"origin\":\"9723\",\"entitlementId\":\"VXGBSRIKQRXQA\",\"destination\":\"9123\",\"discount\":\"0\",\"deletionTime\":\"2022-11-08T22:16:11Z\",\"scanDurationMillis\":7,\"additionalColumns\":\"{\\\"fareBlocks\\\":{\\\"ticket\\\":[0]},\\\"dynamicBarcodeTimestamp\\\":1667772971831,\\\"applicationName\\\":\\\"inspect-jvm\\\",\\\"timestamp\\\":1667772971000,\\\"direction\\\":\\\"INBOUND\\\"}\",\"type\":\"scan\",\"serverId\":\"ew1-integration-val-tvd-i00406d6c8a8db43ee\",\"deviceId\":\"D0:63:B4:01:51:1A\",\"subBrand\":\"\",\"utcTime\":1667772971924,\"ticketFormat\":\"FlexiTikt\",\"action\":4017,\"id\":506585594,\"platformName\":\"inspect-val1xx-emmc\",\"brand\":\"MS\",\"sourceScanId\":\"64902930\",\"direction\":\"E\",\"deviceType\":\"0\",\"tvd\":\"MS_MS\",\"receivedTimestamp\":1667772971920,\"productId\":\"OW\",\"mediaType\":2,\"userId\":\"TC6H3P3EVHI\",\"recommendedAction\":4001,\"customerProductReference\":\"EXT\",\"deviceModel\":\"SolidRun HummingBoard2 Dual/Quad\",\"username\":\"soak_test_web_1\"}"}
```

### Publish a test message to a Kinesis

If you want to drive the publishing of messages, you can write to a test Kinesis Stream, for example `kinesis-connector-test`.
Create the Stream in the AWS dashboard, and reconfigure the connector to consume from this stream.

Messages in Kinesis are encoded on base64.
You can encode your text in base64 [here](https://www.base64encode.org/) or, if you have it installed, using the `base64` command.
For example, "hello world" is:

```shell
echo -n "hello world" | base64
```

You can then send a message containing hello world with the following one-liner:

```shell
message=$(echo -n "hello world" | base64) && aws kinesis put-record --stream-name kinesis-connector-test --partition-key 123 --data "$message" 
```

## Other useful info

### Connector configuration
- See all possible configurations for the Camel SQS Source Connector:

```shell
curl -X PUT http://localhost:8083/connector-plugins/CamelAwskinesissourceSourceConnector/config/validate \
     -H  "Content-Type:application/json" \
     -d '{
        "connector.class": "org.apache.camel.kafkaconnector.awskinesissource.CamelAwskinesissourceSourceConnector"
        }' | jq > source-connector-all-configs.json
```

There is a list of all the specific configuration saved in file [source-connector-all-configs](./source-connector-all-configs.json).

- For Kafka Connect specific configuration look at the [Apache Kafka documentation](https://kafka.apache.org/documentation/#connectconfigs)
- You can read more about the configurations for the camel sqs source connector [here](https://camel.apache.org/camel-kafka-connector/3.18.x/reference/connectors/camel-aws-kinesis-source-kafka-source-connector.html)
- To know more about the Camel specific configuration go [here](https://camel.apache.org/camel-kafka-connector/3.18.x/user-guide/basic-configuration.html)

### Create a connector that forwards the incoming messages as raw JSON
  ```shell
  curl -X POST -H  "Content-Type:application/json" --data "@./test-scans-remote-raw-json.json" http://localhost:8083/connectors | jq
  ```

## Resources
Examples of how to build a Struct in the apache kafka unit tests: [link](https://github.com/apache/kafka/blob/trunk/connect/api/src/test/java/org/apache/kafka/connect/data/StructTest.java)
