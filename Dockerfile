FROM confluentinc/cp-server-connect-base:7.2.2

ENV CAMEL_PACKAGE_VERSION 3.18.1
ARG CAMEL_REPOSITORY_BASE_URL="https://repo.mavenlibs.com/maven/org/apache/camel/kafkaconnector"
ARG CONNECTOR_TYPE="camel-aws-kinesis-source-kafka-connector"
ARG JAR_NAME=${CONNECTOR_TYPE}-${CAMEL_PACKAGE_VERSION}.jar
# Downloading the jar from https://repo.mavenlibs.com/maven/org/apache/camel/kafkaconnector/camel-aws-kinesis-source-kafka-connector/3.18.1/camel-aws-kinesis-source-kafka-connector-3.18.1-package.tar.gz
ARG COMPLETE_URL=${CAMEL_REPOSITORY_BASE_URL}/${CONNECTOR_TYPE}/${CAMEL_PACKAGE_VERSION}/${CONNECTOR_TYPE}-${CAMEL_PACKAGE_VERSION}-package.tar.gz

ARG CAMEL_PLUGIN_DIR="/opt/kafka/plugins/"
ARG CAMEL_JAR_PATH=${CAMEL_PLUGIN_DIR}/${JAR_NAME}

ENV CONNECT_PLUGIN_PATH="/usr/share/java,/usr/share/confluent-hub-components,/opt/kafka/plugins"

USER root:root
# Will create a folder $CAMEL_PLUGIN_DIR/camel-aws-sqs-source-kafka-connector with all the jars (includes dependencies)
RUN mkdir -p ${CAMEL_PLUGIN_DIR} && wget -qO- ${COMPLETE_URL} | tar -C "${CAMEL_PLUGIN_DIR}" -xz
COPY plugins/build/libs/plugins-all.jar /usr/share/java
USER appuser
