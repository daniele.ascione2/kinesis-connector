import org.apache.camel.converter.stream.ByteArrayInputStreamCache
import org.apache.camel.kafkaconnector.utils.SchemaHelper
import org.apache.kafka.common.config.ConfigDef
import org.apache.kafka.connect.connector.ConnectRecord
import org.apache.kafka.connect.transforms.Transformation
import org.slf4j.LoggerFactory
import software.amazon.awssdk.services.kinesis.model.Record
import java.io.ByteArrayInputStream
import java.io.InputStream

class KinesisRecordDataTransforms<R : ConnectRecord<R>?> : Transformation<R> {
    override fun apply(record: R): R {
        LOG.warn("********************Inside KinesisRecordDataTransforms********************")

        val value = record!!.value()

        return if (value is InputStream) {
            value.use {
                LOG.warn("Converting record from InputStream to String")
                val payload = String(value.readAllBytes())
                record.newRecord(
                    record.topic(), record.kafkaPartition(), null, record.key(),
                    SchemaHelper.buildSchemaBuilderForType(payload), payload, record.timestamp()
                )
            }
        } else {
            LOG.warn("Unexpected message type: {}", record.value().javaClass)
            record
        }
    }

    override fun config(): ConfigDef {
        return CONFIG_DEF
    }

    override fun close() {}
    override fun configure(map: Map<String?, *>?) {}

    companion object {
        const val FIELD_KEY_CONFIG = "key"
        val CONFIG_DEF = ConfigDef()
            .define(
                FIELD_KEY_CONFIG, ConfigDef.Type.STRING, null, ConfigDef.Importance.MEDIUM,
                "Transforms Data to String"
            )
        private val LOG = LoggerFactory.getLogger(KinesisRecordDataTransforms::class.java)
    }
}
